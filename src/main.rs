use std::process::Command;

fn main() {
    clear_screen();
    print_commands();

    loop {
        let command = get_command();
        clear_screen();
        print_commands();
        execute_command(&command);
    }
}

fn clear_screen () {
    print!("\x1B[2J\x1B[1;1H");
}

fn print_commands () {
    println!("Commands:");
    println!("  v, battery - show battery level");
    println!("  r, red - set color to red");
    println!("  g, green - set color to green");
    println!("  b, blue - set color to blue");
    println!("  y, yellow - set color to yellow");
    println!("  m, p, magenta, pink - set color to magenta / pink");
    println!("  c, cyan - set color to cyan");
    println!("  w, white - set color to white");
    println!("  o, off - turn off the RGB lighting");
    println!("  q, e, exit - exit the program");
}

fn get_command () -> String {
    use std::io::{stdin,stdout,Write};

    let mut s=String::new();
    print!("Enter your command: ");
    let _=stdout().flush();
    stdin().read_line(&mut s).expect("Did not enter a correct string");
    s.trim().to_string()
}

fn execute_command (command: &str) {
    match command {
        "v" | "battery" => execute_system_command("sudo rivalcfg --battery-level"),
        "r" | "red" => execute_system_command("sudo rivalcfg -c red"),
        "g" | "green" => execute_system_command("sudo rivalcfg -c green"),
        "b" | "blue" => execute_system_command("sudo rivalcfg -c blue"),
        "w" | "white" => execute_system_command("sudo rivalcfg -c white"),
        "m" | "p" | "magenta" | "pink" => execute_system_command("sudo rivalcfg -c FF00FF"),
        "y" | "yellow" => execute_system_command("sudo rivalcfg -c yellow"),
        "c" | "cyan" => execute_system_command("sudo rivalcfg -c 00FFFF"),
        "o" | "off" => execute_system_command("sudo rivalcfg -c black"),
        "q" | "e" | "exit" => std::process::exit(0),
        _ => println!("Invalid command!"),
    }
}

fn execute_system_command (command: &str) {
    let mut command = command.split_whitespace();
    let command_name = command.next().unwrap();
    let command_args = command.collect::<Vec<&str>>();

    let mut child = Command::new(command_name)
        .args(command_args)
        .spawn()
        .expect("failed to execute process");

    child.wait().expect("failed to wait for child process");
}

